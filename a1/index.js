/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function named register which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
    function register(user) {    
        
        let userExist = registeredUsers.includes(user)
        
        if(userExist) {
        alert("Registration failed. Username already exists!")
        }
        else {
        registeredUsers.push(user)
        alert("Thank you for registering!")
        }
    };   
    


/*
    2. Create a function named addFriend which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

/* let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
    
]; 
    let friendsList = [];*/
    
    function addFriend(registeredUser) {

        const foundUser = registeredUsers.includes(registeredUser); 

        if (!foundUser) return alert("User not found!")
        
        if (friendsList.includes(registeredUser)) return alert("User already in your friends list!")
          
        return friendsList.push(registeredUser) && alert("You have added " + registeredUser + " as a friend!")
        /* if(foundUser) {
            friendsList.push(registeredUser);            
            alert("You have added " + registeredUser + " as a friend!")
        }        
        else {  
            alert("User not found!")
        } */
    };
    
/*
    3. Create a function named displayFriends which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    function displayFriends() {
        
        if (friendsList.length > 0) {
        friendsList.forEach(function(friend) {
            console.log(friend);
        })}
        else {
            alert("You currently have " + friendsList.length + " friends. Add one first.");
        }
                
    };    
    

/*
    4. Create a function named displayNumberOfFriends which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/
    function displayNumberOfFriends() {

        let numberOfFriends = friendsList.length        
        
        if (numberOfFriends > 0) {
            alert("You currently have " + numberOfFriends + " friends.")
        }
        else {
            alert("You currently have " + numberOfFriends + " friends. Add one first.");            
        }
    }

/*
    5. Create a function named deleteFriend which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
    /* function deleteFriend(friend) {

        friendsList.includes(friend)
        
        if(friendsList.length < 1) {
            alert("You currently have " + friendsList.length + " friends. Add one first.")
        }
        else {            
            friendsList.splice(num, friend);            
            console.log(friendsList);
            alert("You have unfriended " + friend + ". You will no longer receive message from " + friend + "." )
        }
    }; */
    function deleteFriend(friend) {       

        if (friendsList.length < 1) return alert("You currently have " + friendsList.length + " friends. Add one first.")

        const friendIndex = friendsList.indexOf(friend)
        let deletedFriends;

        if (friendIndex == -1) return alert("Friend not found in your list")

        else {
            deletedFriends = friendsList.splice(friendIndex, 1);
            console.log(friendsList);
            alert("You have unfriended " + deletedFriends + ". You will no longer able to contact deleted friend.");
        }
        
        
    };

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/
